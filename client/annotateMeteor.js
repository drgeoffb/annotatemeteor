/*
** Override jQuery Ajax for Annotor Store function in Meteor JS
** https://bitbucket.org/bchgys/annotatemeteor
**
** Copyright 2014 Geoff Breach
** Dual licensed under the MIT and GPLv3 licenses.
** https://github.com/
**
** Built at: 4:23 pm Monday, 10 November 2014 (GMT+11) 
*/


// put the original ajax function aside in case we need it 
// for something else
var OriginalAjax = $.ajax;

// override the jQuery ajax function for our own nefarious porpoises:
$.ajax = function(url, options) {

	// this takes the 'url' and 'options' that the Annotator Store plugin 
	// throws at jQuery.ajax(), interprets, it, and calls the passed-in 
	// onSuccess() function as appropriate.

	// a request object that we'll throw around later
	var request = {};

	// interpret the way options are set to decipher indended action
	if ((options.type == 'GET') && (options.data === null)) {

		// load ALL annotations from the store. If annotationData:
		// and loadFromStore: are set when the Store plugin is loaded, 
		// this one won't be called.
		
		//console.log('ACTION: Get all annotations:');

		// retrieve all annotations where no project or document is set
	  	var data = Annotations.find().fetch();

	  	// call onSuccess function
	  	options.success(data);

	  	// set status and other return things (are these used anywhere?)
	  	request.status = 200;
	  	request.statusText = 'OK';
	  	request.responseText = JSON.stringify(data);

	  	// return 
		return request;

		// future: consider handling errors by returning an xhr-like
		// object, calling _onError with xhr.status set to an HTTP error code

	} else if ((options.type == 'POST') && (options.data != null) && (!options.data.id)) {
		
		// console.log('ACTION: Create a new annotation:');

		// the 'data' thing comes to us as a JSON string, not an object
		options.data = JSON.parse(options.data);

		// store the annotation (future: test to ensure this worked)
		var id = Annotations.insert(options.data);

		// write the 'id' returned by the db, back to the db object
		Annotations.update({_id: id},{$set: {id: id}});

		// fetch it back again
		var data = Annotations.findOne({id: id});

		// note, those three database hits could have been done in 
		// a single call, but I'm being ultra careful, drawing the 
		// id back out of the database rather than assuming. Also, 
		// this is Meteor, the database is local, in memory, so 
		// those calls are cheap!

	  	// call onSuccess function
	  	options.success(data);

	  	// set status and other return things (are these used anywhere?)
	  	request.status = 200;
	  	request.statusText = 'OK';
	  	request.responseText = JSON.stringify(data);

		return request;

	} else if ((options.type == 'PUT') && (options.data != null)) {
		
		// console.log('ACTION: Update an existing annotation:');

		// the 'data' thing comes to us as a JSON string, not an object
		options.data = JSON.parse(options.data);

		// store the annotation (future: test to ensure this worked)
		var _id = options.data._id;
		delete options.data._id;
		Annotations.update({_id: _id},{$set: options.data});

		// fetch it back again
		var data = Annotations.findOne({id: id});

	  	// call onSuccess function
	  	options.success(data);

	  	// set status and other return things (are these used anywhere?)
	  	request.status = 200;
	  	request.statusText = 'OK';
	  	request.responseText = JSON.stringify(data);

		return request;

	} else if ((options.type == 'DELETE') && (options.data != null)) {
		
		// console.log('ACTION: Delete an existing annotation:');

		// the 'data' thing comes to us as a JSON string, not an object
		options.data = JSON.parse(options.data);

		// store the annotation (future: test to ensure this worked)
		var _id = options.data._id;
		delete options.data._id;
		Annotations.remove({_id: _id});

	  	// call onSuccess function
	  	options.success(options.data);

	  	// set status and other return things (are these used anywhere?)
	  	request.status = 204;
	  	request.statusText = 'NO CONTENT';
	  	request.responseText = '';

		return request;
	
	} else if ((options.type == 'GET') && (options.data != null)) {

		console.log('ACTION: Search annotations for a particular project/document:');

		// load ALL annotations from the store. If annotationData:
		// and loadFromStore: are set when the Store plugin is loaded, 
		// then options.data as passed in is a drop-in query for Mongo 
		// to find matching documents/annotations.
	  	var data = {};
	  	data.rows = Annotations.find(options.data).fetch();

	  	// call onSuccess function
	  	options.success(data);

	  	// set status and other return things (are these used anywhere?)
	  	request.status = 200;
	  	request.statusText = 'OK';
	  	request.responseText = JSON.stringify(data.rows);

	  	// return 
		return request;

		// future: consider handling errors by returning an xhr-like
		// object, calling _onError with xhr.status set to an HTTP error code

	}
	// done!
};
